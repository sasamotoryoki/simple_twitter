package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.MessageService;


@WebServlet(urlPatterns= {"/edit"} )
public class EditServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String id = request.getParameter("id");

		HttpSession session = request.getSession();
		Message editMessage = new Message();
		List<String> errorMessages = new ArrayList<String>();
		Message message = null;

		if(!StringUtils.isEmpty(id) && id.matches("^[0-9]+$")) {
			editMessage.setId(Integer.parseInt(id));
			message = new MessageService().select(editMessage.getId());
		}

		if (message == null) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		Message message = getMessage(request);

		if (isValid(message, errorMessages)) {
			try {
				new MessageService().update(message);
			} catch (NoRowsUpdatedRuntimeException e) {
				errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
			}
		}

		if (errorMessages.size() != 0) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
		}

		session.setAttribute("message", message);
		response.sendRedirect("./");

	}

	private Message getMessage(HttpServletRequest request) throws IOException, ServletException {

		Message message = new Message();
		message.setId(Integer.parseInt(request.getParameter("id")));
		message.setText(request.getParameter("text"));
		return message;
	}

	private boolean isValid(Message message, List<String> errorMessages) {

		String text = message.getText();

		if (StringUtils.isEmpty(text) || (140 < text.length())) {
			errorMessages.add("つぶやきは140文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

}
