package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.Message;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = {"/deleteMessage"})
public class DeleteMessageServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();

		Message message = new Message();
		message.setId(Integer.parseInt(request.getParameter("id")));

		try {
			new MessageService().delete(message);
		} catch (NoRowsUpdatedRuntimeException e) {
			errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
		}

		response.sendRedirect("./");
	}



}
